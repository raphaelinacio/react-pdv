    var LoginComponent = React.createClass({
        render: function() {
          return (
        <div className="col-md-4 col-sm-6 top-100">
        <h1>Acesso</h1>
          <form className="form-horizontal">
            <div className="form-group">
              <div className="">
                <input type="email" className="form-control input-lg" id="inputEmail3" placeholder="BR3ABC"/>
              </div>
            </div>
            <div className="form-group">
              <div className="">
                <input type="password" className="form-control input-lg" id="inputPassword3" placeholder="*******"/>
              </div>
            </div>
            <div className="form-group">
              <div className="">
                <button type="submit" className="btn btn-primary btn-lg btn-block">Entrar</button>
              </div>
            </div>
          </form>
        </div>
          );
        }
      });

      var LogoComponent = React.createClass({
        render() {
          return (
            <div className="col-md-4 col-sm-6 top-100">
              <img className="img-responsive" src="/images/CA-logo-5.png"/>
            </div>
          );
        }
      });


      var Row2 = React.createClass({
          render() {
              return (
                    <div className="col-md-2 col-sm-2"></div>
              );
          }
      })

      var Component = React.createClass({
        render() {
          return (
            <div className="row">
                <Row2/>
                <LogoComponent />
                <LoginComponent />
                <Row2/>
            </div>
          );
        }
      });

      ReactDOM.render(
        <Component />,
        document.getElementById('content')
      );
